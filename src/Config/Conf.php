<?php
/**
 * Created by PhpStorm.
 * User: qingmu-523
 * Date: 2018/10/18
 * Time: 14:22
 */

namespace App\Config;


use Noodlehaus\Config;

class Conf
{
    //存储实例的静态成员变量
    static public $_instance;

    static public $conf;

    //禁止外部实例化
    private function __construct()
    {


    }

    //实例化
    public static function getInstance()
    {
        if (self::$_instance instanceof self) {
            return self::$_instance;
        }

        self::$_instance = new self();
        return self::$_instance;
    }


    //实例化
    public function getConfig()
    {
        //如果不存在数据库连接就创建一个
        if (!self::$conf) {
            $conf = new Config(dirname(dirname(__DIR__)) . '/conf');
            self::$conf = $conf;
        }
        return self::$conf;
    }
}