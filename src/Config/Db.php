<?php
/**
 * Created by PhpStorm.
 * User: qingmu-523
 * Date: 2018/10/17
 * Time: 14:58
 */

namespace App\Config;


use Medoo\Medoo;
use Noodlehaus\Config;
use PDO;
use Predis\Client;

class Db
{

    //存储实例的静态成员变量
    static public $_instance;
    //数据库连接静态变量
    static public $_connectSource_oms;
    //数据库连接静态变量
    static public $_connectSource_tmc;
    //redis连接静态变量
    static public $_connectSource_redis;
    private $conf;

    //禁止外部实例化
    private function __construct()
    {

        $this->conf = Conf::getInstance()->getConfig();
    }

    //实例化
    public static function getInstance()
    {
        if (self::$_instance instanceof self) {
            return self::$_instance;
        }

        self::$_instance = new self();
        return self::$_instance;
    }

    //数据库连接
    public function connect_oms()
    {
        //如果不存在数据库连接就创建一个
        if (!self::$_connectSource_oms) {
            $oms_db = new Medoo([
                'database_type' => 'mysql',
                'database_name' => $this->conf->get("oms.database"),
                'server' => $this->conf->get("oms.host"),
                'username' =>$this->conf->get("oms.user"),
                'password' => $this->conf->get("oms.secret"),
                'charset' => 'utf8',
                'port' => $this->conf->get("oms.port"),
                'logging' => false,
                'option' => [
                    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false
                ],
            ]);
            self::$_connectSource_oms = $oms_db;
        }
        return self::$_connectSource_oms;
    }

    //数据库连接
    public function connect_tmc()
    {
        //如果不存在数据库连接就创建一个
        if (!self::$_connectSource_tmc) {
            $tmc_db = new Medoo([
                'database_type' => 'mysql',
                'database_name' => $this->conf->get("tmc.database"),
                'server' => $this->conf->get("tmc.host"),//192.168.15.205
                'username' => $this->conf->get("tmc.user"),
                'password' => $this->conf->get("tmc.secret"),
                'charset' => 'utf8',
                'port' => $this->conf->get("tmc.port"),
                'logging' => false,
                'option' => [
                    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false
                ],
            ]);
            self::$_connectSource_tmc = $tmc_db;
        }
        return self::$_connectSource_tmc;
    }

    //redis连接
    public function connect_redis()
    {
        //如果不存在数据库连接就创建一个
        if (!self::$_connectSource_redis) {
            $options = array(
//                'profile' => '2.4',
                'prefix'  => 'w-',
            );
            $redis_url = "tcp://".$this->conf->get("redis.host").":".$this->conf->get("redis.port");
            $redis_client = new Client($redis_url, $options);
            $redis_client->auth("eE8ijjin88#$@#$59298##%#555Ydfj%%&103PP4");

            self::$_connectSource_redis = $redis_client;
        }
        return self::$_connectSource_redis;
    }
}