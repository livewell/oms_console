<?php
/**
 * Created by PhpStorm.
 * User: qingmu-523
 * Date: 2018/10/17
 * Time: 15:40
 */

namespace App\Command;


use App\Config\Conf;
use App\Config\Db;
use Faker\Factory;
use Noodlehaus\Config;
use Symfony\Component\Console\Command\Command;

abstract class  BaseCommand extends Command
{

    public $faker;
    public $tmcdb;
    public $omsdb;
    public $config;
    public $redis;
    public $cache;

    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->faker = Factory::create("zh_CN");
        $this->tmcdb = Db::getInstance()->connect_tmc();
        $this->omsdb = Db::getInstance()->connect_oms();
        $this->config = Conf::getInstance()->getConfig();
        $this->redis = Db::getInstance()->connect_redis();
    }


    public function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
    }

    public function getData()
    {
        $skus = $this->config->get("skus");
        //随机sku件数
        $num = $this->faker->numberBetween(1, $this->config->get("per_order_sku_num"));
        $nums = array_rand($skus, $num);
        if (!is_array($nums)) {
            $nums = array($nums);
        }
        $input = array();
        // var_dump("我的数字值");
        // var_dump($nums);
        // if(is_array($nums)){
        foreach ($nums as $value) {
            //随机购买num件数
            $num = $this->faker->numberBetween(1,  $this->config->get("per_sku_num"));
            $item = $skus[$value];
//            var_dump($item);
            $item['num'] = $num;
            $input[] = $item;
        }
        // }
        // var_dump($input);
        return $input;
    }

    public function do_queue($q_name = '',$op, $data = ''){
        $info = [
            "op" => $op,//"add",
            "data" => $data
        ];
        $this->redis->lpush($q_name, json_encode($info, JSON_UNESCAPED_UNICODE));
    }

}