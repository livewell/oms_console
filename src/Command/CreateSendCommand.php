<?php
/**
 * Created by PhpStorm.
 * User: qingmu-523
 * Date: 2019/1/17
 * Time: 10:21
 */

namespace App\Command;


use App\Config\Db;
use Carbon\Carbon;
use PDO;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class CreateSendCommand extends BaseCommand
{

    protected function configure()
    {
        $this
            ->setName('app:create-send')
            ->setDescription('把订单设置为发货状态')
            ->setHelp("把订单设置为发货状态")// configure an argument / 配置一个参数
            ->addOption("order_sn", 'sn', InputOption::VALUE_REQUIRED, "订单号");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $start_time = time();
        $order_sn = $input->getOption('order_sn');

        $wms_rows = $this->omsdb->select("wms_tickets", [
            // Here is the table relativity argument that tells the relativity between the table you want to join.

            // The row author_id from table post is equal the row user_id from table account
            "[>]wms_tickets_goods" => ["id" => "ticket_id"],
        ],  [
            "wms_tickets.id",
            "wms_tickets.type_code","wms_tickets.relation_sn","wms_tickets.original_sn","wms_tickets.sn","wms_tickets.stock_code",
            "wms_tickets_goods.goods_sku_sn","wms_tickets_goods.number_expected","wms_tickets_goods.goods_name"
        ], ["wms_tickets.original_sn" => $order_sn,"status_ticket[!]"=>5]);

//        var_dump($wms_rows);
        if (empty($wms_rows)) {
            die('is empty!');
        }

        $order_delivery_brand = array(
            'order_sn' => $wms_rows[0]['original_sn'],
            'invoice_no' => time(),//订单发货单号
            'delivery_time' => date('Y-m-d H:i:s'),
            'delivery_code' => $this->config->get("delivery_code"),
            'delivery_name' => $this->config->get("delivery_name"),
            'weight' => '0.00',
            'suuplier_sn' => '201602001',//供应商代码
            'delivery_status' => '1',//状态 (1：发货2:签收3:拒收4:异常)
            'take_time' => date('Y-m-d H:i:s'),
            'delivery_id' => $wms_rows[0]['relation_sn'],//发货编号
        );
        $this->omsdb->insert("order_delivery_brand",$order_delivery_brand);
//        $this->db->create_mapper('order_delivery_brand')->insert($order_delivery_brand);
        $api_response_info = array(
            're_name' => '销售出库',
            're_status' => 2,
            're_type' => 3,
            'original_bn' => $wms_rows[0]['sn'],
            'stock_code' => $wms_rows[0]['stock_code'],
            're_time' => date('Y-m-d H:i:s'),
            'last_time' => date('Y-m-d H:i:s'),
            're_function' => 'deliveryorder_confirm',
        );

        $this->omsdb->insert("api_response_info",$api_response_info);
//        $this->db->create_mapper('api_response_info')->insert($api_response_info);

        foreach ($wms_rows as $wms_row) {
            $api_response_goods_info = array(
                'sn' => $wms_row['sn'],
                'sku_sn' => $wms_row['goods_sku_sn'],
                'goods_name' => $wms_row['goods_name'],
                'plan_qty' => $wms_row['number_expected'],
                'actual_qty' => $wms_row['number_expected'],
                'system_code' => $wms_row['stock_code'],
                'unique_code' => $wms_row['sn'],
                'add_time' => date('Y-m-d H:i:s'),
            );
            $this->omsdb->insert("api_response_goods_info",$api_response_goods_info);
//            $this->db->create_mapper('api_response_goods_info')->insert($api_response_goods_info);
        }
//        echo $request['order_sn'];

        $this->do_queue('notice','add',array('ticket'=>array('sn'=>$wms_rows[0]['sn'])));

        $end_time = time();
        $output->writeln("结束时间：" . $end_time);

        $output->writeln("耗时时间：" . ($end_time - $start_time));

        $output->writeln([
            'Order Send Out!!!',
            '============',
            '',
        ]);

        // outputs a message followed by a "\n"
        $output->writeln('Success!');
    }



}