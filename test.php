<?php
/**
 * Created by PhpStorm.
 * User: qingmu-523
 * Date: 2018/10/18
 * Time: 11:38
 */

require __DIR__.'/vendor/autoload.php';

use Medoo\Medoo;
use Noodlehaus\Config;

$conf = new Config(__DIR__ .  '/conf');////每笔订单的最大sku数 //每个sku购买num最大件数

//$oms_host = $conf->get("skus");
//var_dump(json_encode($oms_host,JSON_UNESCAPED_UNICODE));

var_dump($conf->get("per_order_sku_num"));
var_dump($conf->get("per_sku_num"));


$oms_db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => $conf->get("oms.database"),
    'server' => $conf->get("oms.host"),
    'username' =>$conf->get("oms.user"),
    'password' => $conf->get("oms.secret"),
    'charset' => 'utf8',
    'port' => $conf->get("oms.port"),
    'logging' => false,
    'option' => [
        PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false
    ],
]);

$data = $oms_db->query("SELECT t_in.sku_sn, t_in.goods_sn, t_in.actual_number, t_in.available_number, g.product_price, g.goods_name, g.outer_goods_sn FROM t_channel_sku AS t_sku LEFT JOIN t_sku_inventory AS t_in ON t_sku.sku_sn = t_in.sku_sn LEFT JOIN product_goods AS g ON t_in.goods_sn = g.goods_sn WHERE t_in.sku_sn IN ( '2017AW4310135070640', '2017AW4310135070740', '2017AW4310135070840', '2018SS4005030100741', '2018SS4300030211840', '2018SS4300035103240', '16AW4400135015540', '16AW4400135015541', '16AW4400135015740', '16AW4400135105240' )")->fetchAll(PDO::FETCH_ASSOC);

var_dump($data);