#!/usr/bin/env php
<?php
// application.php
 
require __DIR__.'/vendor/autoload.php';

use App\Command\CreateI216XmlSendOut;
use App\Command\CreateI217XmlSalesReturn;
use App\Command\CreateOrderTmcCommand;
use App\Command\CreateReturnTmcCommand;
use App\Command\CreateSendCommand;
use Symfony\Component\Console\Application;
use App\Command;
 
$application = new Application();
 
// ... register commands / 注册命令

$application->add(new CreateOrderTmcCommand());
$application->add(new CreateReturnTmcCommand());
$application->add(new CreateI216XmlSendOut());
$application->add(new CreateI217XmlSalesReturn());
$application->add(new CreateSendCommand());

$application->run();